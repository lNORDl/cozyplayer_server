var db = require('pg-bricks').configure('postgres://postgres:123321@127.0.0.1:5432/DDDBase');
var multer = require('multer');
var session = require('express-session');
var mailer = require('nodemailer');
var fs = require('fs');
var app = null;

var ACCESS_PRIVATE = 10;
var ACCESS_PUBLIC = 90;

var ACCESS_USER = 1;
var ACCESS_ADMIN = 3;

var isBlocked = false;

var transporter = mailer.createTransport({
   service: 'Gmail',
   auth: {
      user: 'nordworker@gmail.com',
      pass: '92nordworker'
   }
});

function sendRegistrationMail(login, email, code)
{
   var mailOptions = {
      from: 'Аудио-Сервис', // sender address
      to: email, // list of receivers
      subject: 'Регистрация в аудио-сервисе', // Subject line
      text: 'Здравствуйте ' + login + '! Код для подтверждения: ' + code + ';' // plaintext body
      //html: '<b>Hello world ✔</b>' // html body
   };

   transporter.sendMail(mailOptions, function(error, info){
      if(error){
         return console.log(error);
      }
      console.log('Registration message send: ' + info.response);
   });
}

function getRandomInt(min, max)
{
   return Math.floor(Math.random() * (max - min + 1)) + min;
}

function sendJson(res, obj) {
   res.header("Content-Type", "application/json; charset=utf-8");
   res.end(JSON.stringify(obj, null, 3));
};

function sendFile(res, fileName)
{
   var options = {
      root: __dirname + '/filestorage/'
   };

   res.sendFile(fileName, options, function (err) {
      if (err) {
         console.log(err);
         res.status(err.status).end();
      }
      else {
         console.log('Sent:', fileName);
      }
   });
}

function printTable(name)
{
   db.select().from(name).rows(function(err, data) {
      if(err) console.log(err);
      console.log(data);
   });
}

function deleteAudiotrack(path)
{
   path = __dirname + '/filestorage/' + path;
   fs.unlink(path, function (err) {
      if (err) console.log(err);//throw err;
      console.log('Audiofile deleted!');
   });
}

function utf8_encode(str_data)
{

   str_data = str_data.replace(/\r\n/g,"\n");
   var utftext = "";

   for (var n = 0; n < str_data.length; n++) {
      var c = str_data.charCodeAt(n);
      if (c < 128) {
         utftext += String.fromCharCode(c);
      } else if((c > 127) && (c < 2048)) {
         utftext += String.fromCharCode((c >> 6) | 192);
         utftext += String.fromCharCode((c & 63) | 128);
      } else {
         utftext += String.fromCharCode((c >> 12) | 224);
         utftext += String.fromCharCode(((c >> 6) & 63) | 128);
         utftext += String.fromCharCode((c & 63) | 128);
      }
   }

   return utftext;
}

function md5(str)
{

   var RotateLeft = function(lValue, iShiftBits) {
      return (lValue<<iShiftBits) | (lValue>>>(32-iShiftBits));
   };

   var AddUnsigned = function(lX,lY) {
      var lX4,lY4,lX8,lY8,lResult;
      lX8 = (lX & 0x80000000);
      lY8 = (lY & 0x80000000);
      lX4 = (lX & 0x40000000);
      lY4 = (lY & 0x40000000);
      lResult = (lX & 0x3FFFFFFF)+(lY & 0x3FFFFFFF);
      if (lX4 & lY4) {
         return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
      }
      if (lX4 | lY4) {
         if (lResult & 0x40000000) {
            return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
         } else {
            return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
         }
      } else {
         return (lResult ^ lX8 ^ lY8);
      }
   };

   var F = function(x,y,z) { return (x & y) | ((~x) & z); };
   var G = function(x,y,z) { return (x & z) | (y & (~z)); };
   var H = function(x,y,z) { return (x ^ y ^ z); };
   var I = function(x,y,z) { return (y ^ (x | (~z))); };

   var FF = function(a,b,c,d,x,s,ac) {
      a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
      return AddUnsigned(RotateLeft(a, s), b);
   };

   var GG = function(a,b,c,d,x,s,ac) {
      a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
      return AddUnsigned(RotateLeft(a, s), b);
   };

   var HH = function(a,b,c,d,x,s,ac) {
      a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
      return AddUnsigned(RotateLeft(a, s), b);
   };

   var II = function(a,b,c,d,x,s,ac) {
      a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
      return AddUnsigned(RotateLeft(a, s), b);
   };

   var ConvertToWordArray = function(str) {
      var lWordCount;
      var lMessageLength = str.length;
      var lNumberOfWords_temp1=lMessageLength + 8;
      var lNumberOfWords_temp2=(lNumberOfWords_temp1-(lNumberOfWords_temp1 % 64))/64;
      var lNumberOfWords = (lNumberOfWords_temp2+1)*16;
      var lWordArray=Array(lNumberOfWords-1);
      var lBytePosition = 0;
      var lByteCount = 0;
      while ( lByteCount < lMessageLength ) {
         lWordCount = (lByteCount-(lByteCount % 4))/4;
         lBytePosition = (lByteCount % 4)*8;
         lWordArray[lWordCount] = (lWordArray[lWordCount] | (str.charCodeAt(lByteCount)<<lBytePosition));
         lByteCount++;
      }
      lWordCount = (lByteCount-(lByteCount % 4))/4;
      lBytePosition = (lByteCount % 4)*8;
      lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80<<lBytePosition);
      lWordArray[lNumberOfWords-2] = lMessageLength<<3;
      lWordArray[lNumberOfWords-1] = lMessageLength>>>29;
      return lWordArray;
   };

   var WordToHex = function(lValue) {
      var WordToHexValue="",WordToHexValue_temp="",lByte,lCount;
      for (lCount = 0;lCount<=3;lCount++) {
         lByte = (lValue>>>(lCount*8)) & 255;
         WordToHexValue_temp = "0" + lByte.toString(16);
         WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length-2,2);
      }
      return WordToHexValue;
   };

   var x=Array();
   var k,AA,BB,CC,DD,a,b,c,d;
   var S11=7, S12=12, S13=17, S14=22;
   var S21=5, S22=9 , S23=14, S24=20;
   var S31=4, S32=11, S33=16, S34=23;
   var S41=6, S42=10, S43=15, S44=21;

   str = utf8_encode(str);
   x = ConvertToWordArray(str);
   a = 0x67452301; b = 0xEFCDAB89; c = 0x98BADCFE; d = 0x10325476;

   for (k=0;k<x.length;k+=16) {
      AA=a; BB=b; CC=c; DD=d;
      a=FF(a,b,c,d,x[k+0], S11,0xD76AA478);
      d=FF(d,a,b,c,x[k+1], S12,0xE8C7B756);
      c=FF(c,d,a,b,x[k+2], S13,0x242070DB);
      b=FF(b,c,d,a,x[k+3], S14,0xC1BDCEEE);
      a=FF(a,b,c,d,x[k+4], S11,0xF57C0FAF);
      d=FF(d,a,b,c,x[k+5], S12,0x4787C62A);
      c=FF(c,d,a,b,x[k+6], S13,0xA8304613);
      b=FF(b,c,d,a,x[k+7], S14,0xFD469501);
      a=FF(a,b,c,d,x[k+8], S11,0x698098D8);
      d=FF(d,a,b,c,x[k+9], S12,0x8B44F7AF);
      c=FF(c,d,a,b,x[k+10],S13,0xFFFF5BB1);
      b=FF(b,c,d,a,x[k+11],S14,0x895CD7BE);
      a=FF(a,b,c,d,x[k+12],S11,0x6B901122);
      d=FF(d,a,b,c,x[k+13],S12,0xFD987193);
      c=FF(c,d,a,b,x[k+14],S13,0xA679438E);
      b=FF(b,c,d,a,x[k+15],S14,0x49B40821);
      a=GG(a,b,c,d,x[k+1], S21,0xF61E2562);
      d=GG(d,a,b,c,x[k+6], S22,0xC040B340);
      c=GG(c,d,a,b,x[k+11],S23,0x265E5A51);
      b=GG(b,c,d,a,x[k+0], S24,0xE9B6C7AA);
      a=GG(a,b,c,d,x[k+5], S21,0xD62F105D);
      d=GG(d,a,b,c,x[k+10],S22,0x2441453);
      c=GG(c,d,a,b,x[k+15],S23,0xD8A1E681);
      b=GG(b,c,d,a,x[k+4], S24,0xE7D3FBC8);
      a=GG(a,b,c,d,x[k+9], S21,0x21E1CDE6);
      d=GG(d,a,b,c,x[k+14],S22,0xC33707D6);
      c=GG(c,d,a,b,x[k+3], S23,0xF4D50D87);
      b=GG(b,c,d,a,x[k+8], S24,0x455A14ED);
      a=GG(a,b,c,d,x[k+13],S21,0xA9E3E905);
      d=GG(d,a,b,c,x[k+2], S22,0xFCEFA3F8);
      c=GG(c,d,a,b,x[k+7], S23,0x676F02D9);
      b=GG(b,c,d,a,x[k+12],S24,0x8D2A4C8A);
      a=HH(a,b,c,d,x[k+5], S31,0xFFFA3942);
      d=HH(d,a,b,c,x[k+8], S32,0x8771F681);
      c=HH(c,d,a,b,x[k+11],S33,0x6D9D6122);
      b=HH(b,c,d,a,x[k+14],S34,0xFDE5380C);
      a=HH(a,b,c,d,x[k+1], S31,0xA4BEEA44);
      d=HH(d,a,b,c,x[k+4], S32,0x4BDECFA9);
      c=HH(c,d,a,b,x[k+7], S33,0xF6BB4B60);
      b=HH(b,c,d,a,x[k+10],S34,0xBEBFBC70);
      a=HH(a,b,c,d,x[k+13],S31,0x289B7EC6);
      d=HH(d,a,b,c,x[k+0], S32,0xEAA127FA);
      c=HH(c,d,a,b,x[k+3], S33,0xD4EF3085);
      b=HH(b,c,d,a,x[k+6], S34,0x4881D05);
      a=HH(a,b,c,d,x[k+9], S31,0xD9D4D039);
      d=HH(d,a,b,c,x[k+12],S32,0xE6DB99E5);
      c=HH(c,d,a,b,x[k+15],S33,0x1FA27CF8);
      b=HH(b,c,d,a,x[k+2], S34,0xC4AC5665);
      a=II(a,b,c,d,x[k+0], S41,0xF4292244);
      d=II(d,a,b,c,x[k+7], S42,0x432AFF97);
      c=II(c,d,a,b,x[k+14],S43,0xAB9423A7);
      b=II(b,c,d,a,x[k+5], S44,0xFC93A039);
      a=II(a,b,c,d,x[k+12],S41,0x655B59C3);
      d=II(d,a,b,c,x[k+3], S42,0x8F0CCC92);
      c=II(c,d,a,b,x[k+10],S43,0xFFEFF47D);
      b=II(b,c,d,a,x[k+1], S44,0x85845DD1);
      a=II(a,b,c,d,x[k+8], S41,0x6FA87E4F);
      d=II(d,a,b,c,x[k+15],S42,0xFE2CE6E0);
      c=II(c,d,a,b,x[k+6], S43,0xA3014314);
      b=II(b,c,d,a,x[k+13],S44,0x4E0811A1);
      a=II(a,b,c,d,x[k+4], S41,0xF7537E82);
      d=II(d,a,b,c,x[k+11],S42,0xBD3AF235);
      c=II(c,d,a,b,x[k+2], S43,0x2AD7D2BB);
      b=II(b,c,d,a,x[k+9], S44,0xEB86D391);
      a=AddUnsigned(a,AA);
      b=AddUnsigned(b,BB);
      c=AddUnsigned(c,CC);
      d=AddUnsigned(d,DD);
   }

   var temp = WordToHex(a)+WordToHex(b)+WordToHex(c)+WordToHex(d);

   return temp.toLowerCase();
}

var ready = function(application)
{
   app = application;
   console.log("INIT");

   app.use(session({ secret: 'random word', cookie: { maxAge: null }}));

   printTable('users');


   app.use(function(req, res, next)
   {
      multer(
          {
             dest: __dirname + '/filestorage/',

             onFileUploadStart: function (file, req, res) {
                console.log('Load file, isBlocked:' + isBlocked);
                if (isBlocked) return false;
             },

             onFileUploadComplete: function (file)
             {
                req.body.filepath = file.path;
                req.body.filesize = file.size;
             }
          })(req, res, next);
   });

   app.post('/api/get_parametrs', function (req, res)
   {
      if(req.session.isLogin && req.session.access == ACCESS_ADMIN)
      {
         sendJson(res, {isBlocked:isBlocked});
      }
      else sendJson(res, {status:'ERROR!'});

   });

   app.post('/api/set_parametrs', function (req, res)
   {
      if(req.session.isLogin && req.session.access == ACCESS_ADMIN)
      {
         var isB = req.body.is_blocked;
         console.log(isB === 'true');
         isBlocked = isB === 'true';
         sendJson(res, {status:'OK'});
      }
      else sendJson(res, {status:'ERROR'});

   });

   // Получить файл по запрошенному пути.
   app.get('/api/get_audiotrack', function (req, res, next)
   {
      sendFile(res, req.query.name);
   });

   app.post('/api/activate', function (req, res)
   {
      var login = req.body.login;
      var code = req.body.code;

      db.query("SELECT * FROM temp_users WHERE login = $1 AND code = $2", [login, code], function(err, data)
      {
         if(data.rows.length != 0)
         {
            var passwordHash = data.rows[0].password_hash;
            var email = data.rows[0].email;

            createUser(login, passwordHash, email, function(err, data)
            {
               if(err) console.log(err);

               db.query("DELETE FROM temp_users WHERE login = $1 AND code = $2", [login, code], function(err, data)
               {
                  if(err) console.log(err);
               });

               console.log('Registration Sucesfull!');
               sendJson(res, {status:'OK', hint:'Активация успешна! Приятного использования!'});
            });
         }
         else sendJson(res, {status:'ERROR', hint:'Неверные данные!'});
      });
   });

   app.post('/api/registration', function (req, res)
   {
      var login = req.body.login;
      var passwordHsh = md5(req.body.password);
      var email = req.body.email;

      db.query("SELECT * FROM users WHERE login = $1 OR email = $2", [login, email], function(err, dataU)
      {
         if (err) console.log(err);

         if(dataU.rows.length == 0)
         {
            db.query("SELECT * FROM temp_users WHERE login = $1 OR email = $2", [login, email], function(err, dataT)
            {
               if (err) console.log(err);

               if(dataT.rows.length == 0)
               {
                  var code = getRandomInt(1000000, 9999999);
                  db.query("INSERT INTO temp_users (login, password_hash, code, date, email) VALUES ($1, $2, $3, $4, $5)", [login, passwordHsh, code, new Date(), email], function(err, dataR)
                  {
                     if (err) console.log(err);
                     else
                     {
                        sendRegistrationMail(login, email, code);
                        sendJson(res, {status:"OK", hint:"Регистрация успешна! Код для подтвержения выслан на указанные Email."});
                     }
                  });
               }
               else sendJson(res, {status:"FAIL", hint:"Пользователь с таким Логином или Email уже зарегистрирован."});
            });
         }
         else sendJson(res, {status:"FAIL", hint:"Пользователь с таким Логином или Email уже зарегистрирован."});
      });
   });

   app.post('/api/do_admin_query', function (req, res)
   {
      if(req.session.isLogin && req.session.access == ACCESS_ADMIN)
      {
         var query = req.body.query;
         db.query(query, [], function(err, data)
         {
            if (err) sendJson(res, err);
            else sendJson(res, data.rows);
         });
      }
   });

   app.post('/api/get_audiotracks_statistics', function (req, res)
   {
      if(req.session.isLogin && req.session.access == ACCESS_ADMIN)
      {
         var dateStart = req.body.date_start;
         var dateEnd = req.body.date_end;

         db.query("SELECT to_char(audiotracks.load_date::date, 'DD.MM.yyyy') AS date, count(audiotracks) AS count FROM audiotracks " +
         "WHERE audiotracks.load_date::date >= $1 AND audiotracks.load_date::date <= $2 " +
         "GROUP BY audiotracks.load_date::date ORDER BY audiotracks.load_date::date", [dateStart, dateEnd], function(err, dataStatistics)
         {
            if(err) console.log(err);

            db.query("SELECT audiotracks.* FROM audiotracks " +
            "WHERE audiotracks.load_date::date >= $1 AND audiotracks.load_date::date <= $2 " +
            "ORDER BY audiotracks.load_date", [dateStart, dateEnd], function(err, dataAudiotracks)
            {
               if(err) console.log(err);
               sendJson(res, {statistics:dataStatistics.rows, audiotracks:dataAudiotracks.rows});
            });
         });
      }
      else sendJson(res, {status:"ERROR"});
   });

   app.post('/api/get_users_statistics', function (req, res)
   {
      if(req.session.isLogin && req.session.access == ACCESS_ADMIN)
      {
         var dateStart = req.body.date_start;
         var dateEnd = req.body.date_end;

         db.query("SELECT to_char(users.date, 'DD.MM.yyyy') AS date, count(users) AS count FROM users " +
                  "WHERE users.date >= $1 AND users.date <= $2 " +
                  "GROUP BY users.date ORDER BY users.date", [dateStart, dateEnd], function(err, data)
         {
            if(err) console.log(err);
            sendJson(res, data.rows);
         });
      }
      else sendJson(res, {status:"ERROR"});
   });

   app.post('/api/get_top_genres', function (req, res)
   {
      var count = req.body.count;
      var dateStart = req.body.date_start;
      var dateEnd = req.body.date_end;

      db.query("SELECT sum(popularity.count) AS count, genres.* FROM genres, popularity, audiotracks " +
      "WHERE popularity.id_audiotrack = audiotracks.id AND audiotracks.id_genre = genres.id AND popularity.date::date >= $2 AND popularity.date <= $3 " +
      "GROUP BY genres.id ORDER BY count DESC LIMIT $1", [count, dateStart, dateEnd], function(err, data)
      {
         if(err) console.log(err);
         sendJson(res, data.rows);
      });

   });

   app.post('/api/get_top_audiotracks', function (req, res)
   {
      var count = req.body.count;
      var dateStart = req.body.date_start;
      var dateEnd = req.body.date_end;

      db.query("SELECT sum(count) AS count, audiotracks.* AS popularity FROM popularity " +
               "LEFT OUTER JOIN audiotracks ON audiotracks.id = popularity.id_audiotrack " +
               "WHERE popularity.date::date >= $2 AND popularity.date <= $3 " +
               "GROUP BY id_audiotrack, audiotracks.id ORDER BY count DESC LIMIT $1", [count, dateStart, dateEnd], function(err, data)
      {
         if(err) console.log(err);
         sendJson(res, data.rows);
      });
   });

   app.post('/api/add_popularity', function (req, res)
   {
      var idAudiotrack = req.body.id_audiotrack;
      var date = req.body.date;
      addPopularity(idAudiotrack, date);

      sendJson(res, {status:'OK'});
   });

   app.post('/api/get_genres', function (req, res)
   {
      db.query("SELECT * FROM genres", [], function (err, data)
      {
         if (err) console.log(err);
         sendJson(res, data.rows);
      });
   });

   app.post('/api/login', function (req, res)
   {
      var log = req.body.login;
      var password = md5(req.body.password);

      login(log, password, function(err, data)
      {
         if(err) console.log(err);
         if(data.rows[0] != undefined)
         {
            var user = data.rows[0];
            var id = user.id;
            var access = user.access;

            req.session.isLogin = true;
            req.session.user = user;
            req.session.userId = id;
            req.session.access = access;

            sendJson(res, {status:"Ok", id:id, access:access});
         }
         else
         {
            sendJson(res, {status:"Fail", id:id});
         }
      });
   });

   app.post('/api/audiotrack_download', function (req, res)
   {
       if(req.session.isLogin)
       {
          var filename = __dirname + 'filestorage/' + req.body.path;
          //res.sendfile(filename, null);
          sendFile(res, req.body.path);
       }
       else
       {
          sendJson(res, {status:'ERROR'});
       }
   });

   // Поиск плейлистов
   app.post('/api/search_playlists', function (req, res)
   {
      if(req.session.isLogin)
      {
         var tags = JSON.parse(req.body.tags);
         var isTags = tags.length != 0;
         var isFree = req.body.is_free;

         var q1 = "SELECT DISTINCT playlists.id, playlists.* FROM playlists, unnest($1::character varying[]) a " +
             "WHERE (playlists.access = $3) " +
             "AND ($2 = false OR (playlists.tags && ($1) OR lower(playlists.name) LIKE ('%' || a || '%'))) " +
             "AND ($4 = false OR playlists.is_password = false)";
         var p1 = [tags, isTags, ACCESS_PUBLIC, isFree];
         var q2 = "SELECT playlists.id, playlists.* FROM playlists " +
             "WHERE (playlists.access = $2) " +
             "AND ($1 = false OR playlists.is_password = false)";
         var p2 = [isFree, ACCESS_PUBLIC];

         var q = (isTags)?q1:q2;
         var p = (isTags)?p1:p2;

         db.query(q, p, function (err, data)
         {
            if (err) console.log(err);
            sendJson(res, data.rows);
         });
      }
      else
      {
         sendJson(res, {status:'ERROR'});
      }
   });

   // Поиск аудиозаписей
   app.post('/api/search_audiotracks', function (req, res)
   {
      if(req.session.isLogin)
      {
         var tags = JSON.parse(req.body.tags);
         var isTags = tags.length != 0;
         var idGenre = req.body.id_genre;
         var date = req.body.date;
         var isGenre = idGenre != -1;
         var isDate = date.length != 0;

         if(!isDate) date = '12-01-1995';

         var q1 = "SELECT audiotracks.id, audiotracks.* FROM audiotracks, unnest($1::character varying[]) a " +
                  "WHERE (audiotracks.is_private = false) " +
                  "AND ($2 = false OR audiotracks.tags && ($1) OR lower(audiotracks.name) LIKE ('%' || a || '%') OR lower(audiotracks.artist) LIKE ('%' || a || '%')) " +
                  "AND ($4 = false OR audiotracks.id_genre = $3) AND ($6 = false OR (audiotracks.load_date)::date = ($5)::date) " +
                  "GROUP BY audiotracks.id ORDER BY audiotracks.load_date::date DESC";
         var p1 = [tags, isTags, idGenre, isGenre, date, isDate];

         var q2 = "SELECT audiotracks.id, audiotracks.* FROM audiotracks " +
                  "WHERE (audiotracks.is_private = false) " +
                  "AND ($2 = false OR audiotracks.id_genre = $1) AND ($4 = false OR (audiotracks.load_date)::date = ($3)::date) " +
                  "GROUP BY audiotracks.id ORDER BY audiotracks.load_date::date DESC";
         var p2 = [idGenre, isGenre, date, isDate];

         var q = (isTags)?q1:q2;
         var p = (isTags)?p1:p2;

         db.query(q, p, function (err, data)
         {
            if (err) console.log(err);
            sendJson(res, data.rows);
         });
      }
      else
      {
         console.log("NotLogin!");
         sendJson(res, {status:'ERROR'});
      }
   });

   // Получить привязанные плейлисты пользователя
   app.post('/api/get_usings_playlists', function (req, res)
   {
      if(req.session.isLogin)
      {
         var userId = req.session.userId;

         db.query("SELECT playlists.* FROM playlists, using_playlists WHERE using_playlists.id_playlist = playlists.id AND using_playlists.id_user = $1", [userId], function (err, data)
         {
            if (err) console.log(err);
            sendJson(res, data.rows);
         });
      }
      else
      {
         sendJson(res, {status:'ERROR'});
      }
   });

   // Отвязать плейлист
   app.post('/api/remove_using_playlist', function (req, res)
   {
      if(req.session.isLogin)
      {
         var idPlaylist = req.body.id_playlist;
         var userId = req.session.userId;

         removeUsingPlaylist(idPlaylist, userId);
      }

      sendJson(res, {status: 'Ok'});
   });

   // Привязать плейлист
   app.post('/api/add_using_playlist', function (req, res)
   {
      if(req.session.isLogin)
      {
         var idPlaylist = req.body.id_playlist;
         var userId = req.session.userId;
         var password = req.body.password;

         addUsingPlaylist(idPlaylist, userId, md5(password));
      }

      sendJson(res, {status: 'Ok'});
   });

   // Отвязать аудиозапись от плейлиста
   app.post('/api/remove_usings_audiotracks', function (req, res)
   {
      var audiotracks = JSON.parse(req.body.audiotracks);
      var idPlaylist = req.body.id_playlist;
      for(var i = 0; i < audiotracks.length; i++) removeUsingAudiotrack(idPlaylist, audiotracks[i]);

      sendJson(res, {status: 'Ok'});
   });

   // Привязать аудиозапись к плейлисту
   app.post('/api/add_usings_audiotracks', function (req, res)
   {
      var audiotracks = JSON.parse(req.body.audiotracks);
      var idPlaylist = req.body.id_playlist;
      for(var i = 0; i < audiotracks.length; i++) addUsingAudiotrack(idPlaylist, audiotracks[i]);

      sendJson(res, {status: 'Ok'});
   });

   // Получить все плейлисты юзера.
   app.post('/api/get_user_playlists', function(req, res)
   {
      if(req.session.isLogin)
      {
         var userId = req.session.userId;
         db.query("SELECT * FROM playlists WHERE playlists.id_user = $1 ORDER BY access, date", [userId], function (err, data)
         {
            if (err) console.log(err);
            sendJson(res, data.rows);
         });
      }
      else sendJson(res, {status:'ERROR'});
   });

   // Получить все аудиотреки плейлиста.
   app.post('/api/get_playlist_audiotracks', function(req, res)
   {
      if(req.session.isLogin)
      {
         var playlistId = req.body.id_playlist;
         var password = req.body.password;
         var userId = req.session.userId;

         db.query("SELECT playlists.* FROM playlists WHERE id = $1", [playlistId], function (err, data)
         {
            if (err) console.log(err);

            var playlist = data.rows[0];
            var isAccess = (playlist.id_user == req.session.userId) || (playlist.is_password == false) || (playlist.password_hash == md5(password));

            if(!isAccess)
            {
               db.query("SELECT * FROM using_playlists WHERE id_playlist = $1 AND id_user = $2", [playlistId, userId], function (err, data)
               {
                  if (err) console.log(err);

                  if(data.rows.length != 0)
                  {
                     var usingPassword = data.rows[0].password_hash;
                     if(playlist.password_hash == usingPassword) isAccess = true;
                  }

                  sendContent()
               });
            }
            else sendContent();

            function sendContent()
            {
               if(isAccess)
               {
                  db.query("SELECT audiotracks.* " +
                  "FROM audiotracks, using_audiotracks " +
                  "WHERE using_audiotracks.id_audiotrack = audiotracks.id AND using_audiotracks.id_playlist = $1 " +
                  "ORDER BY using_audiotracks.date ", [playlistId], function (err, data)
                  {
                     if (err) console.log(err);
                     sendJson(res, {data:data.rows});
                  });
               }
               else sendJson(res, {status:'ERROR'});
            }
         });
      }
      else sendJson(res, {status:'ERROR'});
   });

   // Получить использование/связь плейлистов и аудиотреков пользователя.
   app.post('/api/get_user_usings_audiotracks', function(req, res)
   {
      if(req.session.isLogin)
      {
         var userId = req.session.userId;
         db.query("SELECT using_audiotracks.id_audiotrack, using_audiotracks.id_playlist " +
         "FROM audiotracks, playlists, using_audiotracks " +
         "WHERE using_audiotracks.id_audiotrack = audiotracks.id AND using_audiotracks.id_playlist = playlists.id " +
         "AND playlists.id_user = $1", [userId], function (err, data)
         {
            if (err) console.log(err);
            sendJson(res, data.rows);
         });
      }
      else
      {
         sendJson(res, {status:'ERROR'});
      }
   });

   app.post('/api/get_audiotrack_info', function(req, res)
   {
      var audioId = req.body.id_audiotrack;
      db.query("SELECT * FROM audiotracks WHERE id = $1", [audioId], function(err, data)
      {
         if(err) console.log(err);
         sendJson(res, data.rows[0]);
      });
   });

   app.post('/api/remove_audiotrack', function (req, res)
   {
      if (req.session.isLogin && req.session.access == ACCESS_ADMIN)
      {
         var idAudiotrack = req.body.id_audiotrack;
         removeAudiotrack(idAudiotrack, function (err, data)
         {
            if(err) console.log(err);
            sendJson(res, {status: 'OK'});
         });
      }
      else sendJson(res, {status:'ERROR'});
   });

   app.post('/api/add_audiotrack', function (req, res)
   {
      if(req.session.isLogin && !isBlocked)
      {
         var name = req.body.name;
         var artist = req.body.artist;
         var duration = req.body.duration;
         var tags = JSON.parse(req.body.tags);
         var path = req.body.filepath;
         var idUser = req.session.userId;
         var isPrivate = req.body.isPrivate;
         var idGenre = req.body.id_genre;

         path = path.substring(path.lastIndexOf('\\')+1, path.length);
         console.log(path);
         addAudiotrack(name, artist, duration, tags, path, idUser, isPrivate, idGenre, function (err, data)
         {
            if (err) console.log(err);
            else sendJson(res, {data: data});
         });
      }
      else sendJson(res, {status:'ERROR'});
   });

   app.post('/api/add_playlist', function(req, res)
   {
      if(req.session.isLogin)
      {
         var userId = req.session.userId;
         var name = req.body.name;
         var isRoot = false;
         var access = req.body.access;
         var isPassword = req.body.is_password;
         var passwoedHash = md5(req.body.password);
         var tags = JSON.parse(req.body.tags);
         addPlaylist(userId, name, isRoot, access, isPassword, passwoedHash, tags, function(err, data)
         {
            if(err)
            {
               console.log(err);
               sendJson(res, {status:'ERROR'});
            }
            else sendJson(res, data);
         });
      }
      else sendJson(res, {status:'ERROR'});
   });

   app.post('/api/remove_playlist', function(req, res)
   {
      if(req.session.isLogin)
      {
         var userId = req.session.userId;
         var playlistId = req.body.playlist_id;

         removePlaylist(playlistId, function(err, data)
         {
            if(err) console.log(err);
            else sendJson(res, {status:"Ok"});
         });
      }
      else sendJson(res, {status:'ERROR'});
   });

   app.post('/api/edit_playlist', function(req, res)
   {
      if(req.session.isLogin)
      {
         var idPlaylist = req.body.id_playlist;
         var name = req.body.name;
         var access = req.body.access;
         var isPassword = req.body.is_password;
         var password_hash = null;
         var tags = JSON.parse(req.body.tags);

         var isEditPass = req.body.is_change_password;
         if (isEditPass) password_hash = md5(req.body.password);

         editPlaylist(idPlaylist, name, access, isPassword, password_hash, tags, function (err, data)
         {
            if (err) console.log(err);
            sendJson(res, data.rows[0]);
         });
      }
   });
}
//====================================================================================================================//
function addPopularity(idAudiotrack, date)
{
   db.query("SELECT * FROM popularity WHERE id_audiotrack = $1 AND date = $2", [idAudiotrack, date], function (err, data)
   {
      if (err) console.log(err);
      if(data.rows.length == 0)
      {
         db.insert('popularity',
             {
                id_audiotrack: idAudiotrack,
                date: date,
                count: 1
             }).returning('*').row(function(err, data)
             {
                if (err) console.log(err);
                else console.log("Popularity Added!");
             });
      }
      else
      {
         var count = data.rows[0].count + 1;
         var id = data.rows[0].id;

         db.query("UPDATE popularity SET count = $1 WHERE id = $2", [count, id], function (err, data)
         {
            if (err) console.log(err);
            //else printTable('popularity');
         });
      }
   });
}

function addGenre(name, callback)
{
   db.insert('genres',
       {
          name: name
       }).returning('*').row(function(err, data)
       {
          console.log("Genre Added!");
          callback(err, data);
       });
}

function login(log, password, callback)
{
   db.query("SELECT * FROM users WHERE login = $1 AND password_hash = $2", [log, password], function(err, data)
   {
      callback(err, data);
   });
}

function removeAudiotrack(idAudiotrack, callback)
{
   db.query("SELECT * FROM audiotracks WHERE id = $1", [idAudiotrack], function(err, data)
   {
      if(data.rows.length > 0) {
         var path = data.rows[0].path;
         db.query("DELETE FROM using_audiotracks WHERE id_audiotrack = $1", [idAudiotrack], function (err, data) {
            db.query("DELETE FROM popularity WHERE id_audiotrack = $1", [idAudiotrack], function (err, data) {
               db.query("DELETE FROM audiotracks WHERE id = $1", [idAudiotrack], function (err, data) {
                  deleteAudiotrack(path);
                  console.log("Audiotrack Removed!");
                  callback(err, data);
               });
            });
         });
      }
   });
}

function addAudiotrack(name, artist, duration, tags, path, idUser, isPrivate, idGenre, callback)
{
   db.insert('audiotracks', {
      name: name,
      artist: artist,
      duration: duration,
      tags: tags,
      path: path,
      load_date: new Date(),
      id_user: idUser,
      is_private: isPrivate,
      id_genre: idGenre
   }).returning('*').row(function(err, data)
   {
      console.log("Audio Added!");
      callback(err, data);
   });
}

function createUser(log, pass, email, callback)
{
   addUser(log, pass, email, function(err, data)
   {
      if(err) callback(err, data);
      else
      {
         addPlaylist(data.id, 'Все аудиозаписи', true, ACCESS_PRIVATE, false, '', ['Все аудиозаписи'], function(err, data)
         {
            callback(err, data);
         });
      }
   });
}

function addUser(log, pass, email, callback)
{
   db.insert('users',
   {
      login: log,
      password_hash: pass,
      email: email,
      access: ACCESS_USER,
      date: new Date()
   }).returning('*').row(function(err, data)
   {
      console.log("User Added!");
      callback(err, data);
   });
}

function addPlaylist(id_user, name, isRoot, access, isPassword, passwordHash, tags, callback)
{
   db.insert('playlists', {
      id_user: id_user,
      name: name,
      is_root: isRoot,
      access: access,
      is_password: isPassword,
      password_hash: passwordHash,
      date: new Date(),
      tags: tags
   }).returning('*').row(function(err, data)
   {
      console.log("Playlist Added!");
      callback(err, data);
   });
}

function removePlaylist(id_playlist, callback)
{
   db.query("DELETE FROM using_audiotracks WHERE id_playlist = $1", [id_playlist], function(err, data)
   {
      db.query("DELETE FROM playlists WHERE id = $1", [id_playlist], function(err, data)
      {
         console.log("Playlist Removed!");
         callback(err, data);
      });
   });
}

function editPlaylist(id_playlist, name, access, isPassword, passwordHash, tags, callback)
{
   var fullQ = "UPDATE playlists SET name = $2, access = $3, is_password = $4, password_hash = $5, tags = $6 WHERE id = $1";
   var partQ = "UPDATE playlists SET name = $2, access = $3, is_password = $4, tags = $5 WHERE id = $1";
   var fullP = [id_playlist, name, access, isPassword, passwordHash, tags];
   var partP = [id_playlist, name, access, isPassword, tags];

   var q = fullQ;
   if(passwordHash == null) q = partQ;

   var p = fullP;
   if(passwordHash == null) p = partP;

   db.query(q, p, function(err, data)
   {
      if(err) callback(err, data);
      else
      {
         if(access == ACCESS_PRIVATE)
         {
            db.query("DELETE FROM using_playlists WHERE id_playlist = $1", [id_playlist], function (err, data)
            {
               if(err) console.log(err);
               end();
            });
         }
         else end();

         function end()
         {
            db.query("SELECT * FROM playlists WHERE id = $1", [id_playlist], function (err, data)
            {
               console.log("Playlist Edited!");
               callback(err, data);
            });
         }
      }
   });
}

function addUsingPlaylist(idPlaylist, idUser, password)
{
   removeUsingPlaylist(idPlaylist, idUser);

   db.insert('using_playlists',
       {
          id_playlist: idPlaylist,
          id_user: idUser,
          password_hash: password,
          date: new Date()
       }).returning('*').row(function(err, data)
       {
          if(err) console.log(err);
          console.log("UsingPlaylist added!");
       });
}

function removeUsingPlaylist(idPlaylist, idUser)
{
   db.query("DELETE FROM using_playlists WHERE id_playlist = $1 AND id_user = $2", [idPlaylist, idUser], function(err, data)
   {
      if(err) console.log(err);
      console.log("UsingPlaylist deleted!");
   });
}

function addUsingAudiotrack(id_playlist, id_audiotrack)
{
   removeUsingAudiotrack(id_playlist, id_audiotrack);

   db.insert('using_audiotracks',
   {
      id_playlist: id_playlist,
      id_audiotrack: id_audiotrack,
      date: new Date()
   }).returning('*').row(function(err, data)
   {
      if(err) console.log(err);
      console.log("Using added!");
   });
}

function removeUsingAudiotrack(id_playlist, id_audiotrack)
{
   db.query("DELETE FROM using_audiotracks WHERE id_playlist = $1 AND id_audiotrack = $2", [id_playlist, id_audiotrack], function(err, data)
   {
      if(err) console.log(err);
      console.log("Using deleted!");
   });
}

module.exports = { callback: ready };